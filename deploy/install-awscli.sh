apk update && apk add --no-cache curl gcompat groff zip jq

# AWS CLI V2
curl -s https://awscli.amazonaws.com/awscli-exe-linux-x86_64-2.1.39.zip -o awscliv2.zip
unzip awscliv2.zip
aws/install
chmod +x /usr/local/bin/aws

# Lightsail module
curl https://s3.us-west-2.amazonaws.com/lightsailctl/latest/linux-amd64/lightsailctl -o /usr/local/bin/lightsailctl
chmod +x /usr/local/bin/lightsailctl
