import pytest

from api import app


@pytest.fixture
def client():
    # Create a client that we can use to communicate with the API
    with app.test_client() as testing_client:
            with app.app_context():
                yield testing_client

def test_simple_math():
    assert 1 + 1 == 2

def test_hello_world_route(client):
    response = client.get("/")
    print(response)
    assert response.data == b'Hello World from Cal!'
