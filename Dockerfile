FROM python:3.9-slim

EXPOSE 8080

COPY Pipfile .
COPY Pipfile.lock .

# During image building process
RUN pip install micropipenv
RUN micropipenv install

COPY api.py .
COPY static/ static/

# User sees when they start up the container
CMD waitress-serve --port=8080 --call api:create_app