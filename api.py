from flask import Flask
from flask import send_from_directory

app = Flask(__name__)


@app.route('/')
def hello_world():  # put application's code here
    """Returns hello world when you GET / (the root)"""
    return 'Hello World from Cal!'

@app.route("/hello")
def another_hello():
    """Returns a simple response"""
    return 'World'

@app.route('/favicon.ico')
def serve_favicon():
    """Returns the favicon.ico file in the static folder"""
    return send_from_directory('static', 'favicon.ico')


def create_app():
    """Returns the Flask application itself (to be used with a WSGI like Waitress for production)"""
    return app


if __name__ == '__main__':
    app.run()
